package net.selleukidd.banmanager;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Created by marcel on 25.05.17.
 */
public class BanManager {
    private static Connection connection;
    private static ProxiedPlayer p;
    private static Timestamp t;

    public BanManager(Connection connection) {
        this.connection = connection;
        this.t = new Timestamp(System.currentTimeMillis());
    }

    public static void tempBan(ProxiedPlayer sender, String name, String time, String reason) {
        switch (time.split("")[time.length() - 1]) {
            case "d":

                p = ProxyServer.getInstance().getPlayer(name);
                try {
                    Integer expire = Integer.getInteger(time.split("d")[0]);
                    PreparedStatement ps = connection.prepareStatement("INSERT INTO `bans` (`id`, `uuid`, `executer`, `type`, `timestamp`, `expire`) VALUES (NULL, ?, ?, ?, ?, ?)");
                    ps.setString(1, p.getUniqueId().toString());
                    ps.setString(2, sender.getUniqueId().toString());
                    ps.setString(3, "temp");
                    ps.setInt(4, (int) t.getTime());
                    ps.setInt(5, (int) t.getTime());
                    ps.execute();
                    p.getPendingConnection().disconnect(new TextComponent("§6» §dSELLEUKIDD.NET\n §cDu wurdest vom Netzwerk gebannnt!\n§2Grund: §7" + reason + "\n§9Dauer: "));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case "h":
                p = ProxyServer.getInstance().getPlayer(name);
                try {
                    PreparedStatement ps = connection.prepareStatement("INSERT INTO 'bans' ('id', 'uuid', 'executer', 'type', 'timestamp', 'expire') VALUES (NULL, ?, ?, ?, ?, ?)");
                    ps.setString(1, p.getUniqueId().toString());
                    ps.setString(2, sender.getUniqueId().toString());
                    ps.setString(3, "temp");
                    ps.setLong(4, t.getTime());
                    ps.setLong(5, t.getTime() + (3600 * Integer.getInteger(time.split("h")[0])));
                    if (ps.execute()) {
                        p.getPendingConnection().disconnect(new TextComponent("§6» §dSELLEUKIDD.NET\n §cDu wurdest vom Netzwerk gebannnt!\n§2Grund: §7" + reason + "\n§9Dauer: "));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case "m":
                p = ProxyServer.getInstance().getPlayer(name);
                try {
                    PreparedStatement ps = connection.prepareStatement("INSERT INTO 'bans' ('id', 'uuid', 'executer', 'type', 'timestamp', 'expire') VALUES (NULL, ?, ?, ?, ?, ?)");
                    ps.setString(1, p.getUniqueId().toString());
                    ps.setString(2, sender.getUniqueId().toString());
                    ps.setString(3, "temp");
                    ps.setLong(4, t.getTime());
                    ps.setLong(5, t.getTime() + (2629746 * Integer.getInteger(time.split("m")[0])));
                    if (ps.execute()) {
                        p.getPendingConnection().disconnect(new TextComponent("§6» §dSELLEUKIDD.NET\n §cDu wurdest vom Netzwerk gebannnt!\n§2Grund: §7" + reason + "\n§9Dauer: "));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            default:
                sender.sendMessage(new TextComponent("§cBitte nutze die vorgeschriebene Syntax"));
                break;
        }
    }

    public static void permBan(CommandSender sender, String name, String reason) {

    }
}
