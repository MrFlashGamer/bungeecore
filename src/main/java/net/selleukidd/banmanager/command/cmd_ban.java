package net.selleukidd.banmanager.command;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;
import net.selleukidd.banmanager.BanManager;


/**
 * Created by Marcel on 04.06.2017.
 */
public class cmd_ban extends Command {
    public cmd_ban() {
        super("ban");
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        if (strings.length >= 2) {
            BanManager.permBan(sender, strings[0], strings[1]);
        } else {
            sender.sendMessage(new TextComponent("§cBitte nutze die vorgeschriebene Syntax"));
        }
    }
}
