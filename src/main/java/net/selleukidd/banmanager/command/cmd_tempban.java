package net.selleukidd.banmanager.command;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.selleukidd.banmanager.BanManager;

/**
 * Created by Marcel on 28.05.2017.
 */
public class cmd_tempban extends Command {
    public cmd_tempban() {
        super("tempban");
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        if (strings.length >= 3) {
            BanManager.tempBan((ProxiedPlayer) sender, strings[0], strings[1], strings[2]);
        } else {
            sender.sendMessage(new TextComponent("§cBitte nutze die vorgeschriebene Syntax"));
        }
    }
}
