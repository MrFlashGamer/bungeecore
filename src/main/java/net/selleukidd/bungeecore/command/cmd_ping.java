package net.selleukidd.bungeecore.command;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by marcel on 17.05.17.
 */
public class cmd_ping extends Command {


    public cmd_ping() {
        super("ping", null);
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        ProxiedPlayer p = (ProxiedPlayer) commandSender;
        p.sendMessage(new TextComponent("Dein Ping: " + Integer.toString(p.getPing()) + "ms"));
    }
}