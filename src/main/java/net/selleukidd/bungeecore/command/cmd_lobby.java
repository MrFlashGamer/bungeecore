package net.selleukidd.bungeecore.command;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by marcel on 17.05.17.
 */
public class cmd_lobby extends Command {

    public cmd_lobby() {
        super("lobby", null, "l", "hub");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        String[] s = new String[0];
        ProxiedPlayer p = (ProxiedPlayer) commandSender;
        p.connect(ProxyServer.getInstance().getServerInfo("lobby"));
    }
}
