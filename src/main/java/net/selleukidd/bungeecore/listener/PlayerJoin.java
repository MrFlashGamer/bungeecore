package net.selleukidd.bungeecore.listener;

import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by marcel on 17.05.17.
 */
public class PlayerJoin implements Listener {
    @EventHandler
    public void onPlayerJoin(ServerConnectedEvent e) {
        ProxiedPlayer p = e.getPlayer();
        p.setTabHeader(new TextComponent("§6» §dSELLEUKIDD.NET §6| §eExo, Epona, TubeTown und mehr..."), new TextComponent("§2Homepage: §7www.selleukidd.net\n§8You§cTube: §7youtube.selleukidd.net\n§9Discord: §7discord.selleukidd.net\n §eServer: §7" + e.getServer().getInfo().getName()));
    }
}
