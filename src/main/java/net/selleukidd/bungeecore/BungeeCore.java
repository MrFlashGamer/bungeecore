package net.selleukidd.bungeecore;

import net.md_5.bungee.api.plugin.Plugin;
import net.selleukidd.banmanager.BanManager;
import net.selleukidd.banmanager.command.cmd_tempban;
import net.selleukidd.bungeecore.command.cmd_lobby;
import net.selleukidd.bungeecore.command.cmd_ping;
import net.selleukidd.bungeecore.listener.PlayerJoin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by marcel on 16.05.17.
 */
public class BungeeCore extends Plugin {
    private static Connection conn;
    private static BanManager banManager;

    @Override
    public void onEnable() {
        loadMySQL();
        loadListeners();
        loadCommands();
        loadBanManager();
    }


    @Override
    public void onDisable() {

    }

    private void loadMySQL() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/servercore?"
                    + "user=servercore&password=tYCvWvCu780ROAkG");
            getLogger().config("MySql Connection successfully!");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void loadListeners() {
        getProxy().getPluginManager().registerListener(this, new PlayerJoin());
    }

    private void loadCommands() {
        getProxy().getPluginManager().registerCommand(this, new cmd_lobby());
        getProxy().getPluginManager().registerCommand(this, new cmd_ping());
        getProxy().getPluginManager().registerCommand(this, new cmd_tempban());
    }

    private void loadBanManager() {
        banManager = new BanManager(conn);
    }

    public static Connection getMySql() {
        return conn;
    }

    public static BanManager getBanManager() {
        return banManager;
    }
}
